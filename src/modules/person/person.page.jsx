import React from 'react'
import { Provider } from 'mobx-react'

import Store from './store'

import PersonList from './personList'
import styles from './person.less'
const store = new Store()

class Person extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <div className={styles.box}>
                    <PersonList />
                </div>
            </Provider>
        )
    }
}

Person.defaultProps = {}
export default Person
