import React from 'react'
import { toJS } from 'mobx'
import { inject, observer } from 'mobx-react'
import { Table, Divider, Card, Button, Modal } from 'antd'
import PersonModal from './personModal'
import styles from './person.less'

const confirm = Modal.confirm

@inject('store')
@observer
class PersonList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            isEdit: false,
            curPersonDetail: null
        }
    }

    componentDidMount() {
        this.props.store.getPersonList()
    }

    // 添加人员
    onPersonAdd = () => {
        this.setState({
            visible: true
        })
    }

    // 编辑人员
    onPersonEdit = record => {
        this.setState({
            visible: true,
            isEdit: true,
            curPersonDetail: record
        })
    }

    // 删除人员
    onPersonDelete = number => {
        const { store } = this.props
        confirm({
            title: '删除数据',
            content: '确定删除此条数据吗?',
            onOk() {
                store.personDelete(number)
            }
        })
    }

    // 确认人员编辑
    handleOk = value => {
        this.props.store.personModify(value)
        this.setState({
            visible: false,
            isEdit: false,
            curPersonDetail: null
        })
    }

    // 取消人员编辑
    handleCancel = () => {
        this.setState({
            visible: false,
            isEdit: false,
            curPersonDetail: null
        })
    }

    render() {
        const store = this.props.store
        const { visible, isEdit, curPersonDetail } = this.state
        const personList = toJS(store.personList)
        const dataSource = personList

        const modalProps = {
            isEdit,
            visible,
            curPersonDetail,
            handleCancel: this.handleCancel,
            handleOk: this.handleOk
        }
        const columns = [
            {
                title: '编号',
                key: 'number',
                dataIndex: 'number',
                render: text => <span>{text}</span>
            },
            {
                title: '姓名',
                key: 'name',
                dataIndex: 'name',
                render: text => <span>{text}</span>
            },
            {
                title: '性别',
                key: 'sex',
                dataIndex: 'sex',
                render: text => <span>{text}</span>
            },
            {
                title: '描述',
                key: 'remark',
                dataIndex: 'remark',
                render: text => <span>{text}</span>
            },
            {
                title: '操作',
                key: 'operator',
                dataIndex: 'operator',
                render: (text, record) => {
                    return (
                        <div>
                            <a onClick={this.onPersonEdit.bind(this, record)}>
                                编辑
                            </a>
                            <Divider type="vertical" />
                            <a
                                onClick={this.onPersonDelete.bind(
                                    this,
                                    record.number
                                )}
                            >
                                删除
                            </a>
                        </div>
                    )
                }
            }
        ]
        return (
            <Card>
                <Button
                    className={styles.add}
                    type="dashed"
                    icon="plus"
                    onClick={this.onPersonAdd}
                >
                    添加
                </Button>
                <Table
                    rowKey={record => record.name}
                    bordered
                    columns={columns}
                    dataSource={dataSource}
                    pagination={false}
                />
                {visible ? <PersonModal {...modalProps} /> : ''}
            </Card>
        )
    }
}

PersonList.defaultProps = {}
export default PersonList
