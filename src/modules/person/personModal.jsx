import React from 'react'
import { inject, observer } from 'mobx-react'
import { Modal, Form, Input, Select } from 'antd'

const FormItem = Form.Item
const { Option } = Select
const { TextArea } = Input
@inject('store')
@observer
class PersonModal extends React.Component {
    // 新增/编辑完成
    handleOk = () => {
        this.props.form.validateFieldsAndScroll((err, value) => {
            if (!err) {
                const { curPersonDetail, handleOk } = this.props
                const modalData = value
                Object.assign(modalData, {
                    number: curPersonDetail ? curPersonDetail.number : ''
                })
                handleOk(modalData)
            }
        })
    }

    render() {
        const { visible, isEdit, curPersonDetail, handleCancel } = this.props
        const { getFieldDecorator } = this.props.form
        const formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 20 }
        }
        return (
            <Modal
                title={isEdit ? '人员编辑' : '人员新增'}
                visible={visible}
                onOk={this.handleOk}
                onCancel={handleCancel}
            >
                <Form>
                    <FormItem {...formItemLayout} label="姓名">
                        {getFieldDecorator('name', {
                            rules: [
                                {
                                    required: true,
                                    message: '请输入名称'
                                }
                            ],
                            initialValue: isEdit
                                ? (curPersonDetail && curPersonDetail.name) ||
                                  ''
                                : ''
                        })(<Input placeholder="请输入名称" />)}
                    </FormItem>
                    <FormItem {...formItemLayout} label="性别">
                        {getFieldDecorator('sex', {
                            rules: [
                                {
                                    required: true,
                                    message: '请选择性别'
                                }
                            ],
                            initialValue: isEdit
                                ? (curPersonDetail && curPersonDetail.sex) ||
                                  undefined
                                : undefined
                        })(
                            <Select placeholder="请选择性别">
                                <Option value="男">男</Option>
                                <Option value="女">女</Option>
                            </Select>
                        )}
                    </FormItem>
                    <FormItem {...formItemLayout} label="描述">
                        {getFieldDecorator('remark', {
                            initialValue: isEdit
                                ? (curPersonDetail && curPersonDetail.remark) ||
                                  ''
                                : ''
                        })(<TextArea rows={4} placeholder="请输入描述" />)}
                    </FormItem>
                </Form>
            </Modal>
        )
    }
}

export default Form.create({})(PersonModal)
