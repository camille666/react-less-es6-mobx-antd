import { observable, action, runInAction, toJS } from 'mobx'
import axios from 'axios'

class Store {
    @observable personList = []

    @action async getPersonList() {
        const res = await axios.get('/api/personList')
        if (res.data.success) {
            const data = res.data.data
            runInAction(() => {
                this.personList = data
            })
        }
    }

    @action async personDelete(number) {
        const res = await axios.post('/api/delPerson', { number: number })
        if (res.data.success) {
            runInAction(() => {
                this.personList = toJS(this.personList).filter(
                    d => d.number !== number
                )
            })
        }
    }

    @action async personModify(value) {
        const res = await axios.post('/api/updatePerson', {
            data: JSON.stringify(value)
        })
        if (res.data.success) {
            runInAction(() => {
                const editObj = toJS(this.personList).find(
                    d => d.number === value.number
                )
                if (editObj) {
                    this.personList.forEach(item => {
                        if (item.number === value.number) {
                            Object.assign(item, {
                                name: value.name,
                                sex: value.sex,
                                remark: value.remark
                            })
                        }
                    })
                } else {
                    let maxNum
                    if (this.personList.length) {
                        maxNum = Math.max.apply(
                            Math,
                            this.personList.map(function(o) {
                                return o.number
                            })
                        )
                    } else {
                        maxNum = 0
                    }
                    Object.assign(value, { number: Number(maxNum + 1) })
                    this.personList.push(value)
                }
            })
        }
    }
}

export default Store
