import React from 'react'
import { NavLink, Route, Switch } from 'react-router-dom'
import { Layout, Menu, Icon } from 'antd'
import PersonHome from './person/person.page'
import ProjectHome from './project/project.page'
const { Header, Sider, Content } = Layout

export default class HomePage extends React.Component {
    state = {
        collapsed: false
    }

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed
        })
    }

    render() {
        return (
            <Layout>
                <Sider
                    trigger={null}
                    collapsible
                    collapsed={this.state.collapsed}
                >
                    <div className="logo" />
                    <Menu
                        theme="dark"
                        mode="inline"
                        defaultSelectedKeys={['1']}
                    >
                        <Menu.Item key="1">
                            <Icon type="user" />
                            <span>
                                <NavLink to="/manage/personHome">
                                    人员管理
                                </NavLink>
                            </span>
                        </Menu.Item>
                        <Menu.Item key="2">
                            <Icon type="video-camera" />
                            <span>
                                <NavLink to="/manage/projectHome">
                                    项目管理
                                </NavLink>
                            </span>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout>
                    <Header style={{ background: '#fff', padding: 0 }}>
                        <Icon
                            className="trigger"
                            type={
                                this.state.collapsed
                                    ? 'menu-unfold'
                                    : 'menu-fold'
                            }
                            onClick={this.toggle}
                        />
                    </Header>
                    <Content
                        style={{
                            margin: '24px 16px',
                            padding: 24,
                            background: '#fff',
                            minHeight: 280
                        }}
                    >
                        <Switch>
                            <Route
                                exact={true}
                                path="/manage/personHome"
                                component={PersonHome}
                            />
                            <Route
                                exact={true}
                                path="/manage/projectHome"
                                component={ProjectHome}
                            />
                        </Switch>
                    </Content>
                </Layout>
            </Layout>
        )
    }
}
