import React from 'react'
import { toJS } from 'mobx'
import { inject, observer } from 'mobx-react'
import {
    Table,
    Form,
    Popconfirm,
    InputNumber,
    Input,
    message,
    Button,
    Card
} from 'antd'
import styles from './project.less'
const FormItem = Form.Item
const EditableContext = React.createContext()

const EditableRow = ({ form, index, ...props }) => (
    <EditableContext.Provider value={form}>
        <tr {...props} />
    </EditableContext.Provider>
)

const EditableFormRow = Form.create()(EditableRow)

class EditableCell extends React.Component {
    getInput = () => {
        if (this.props.inputType === 'number') {
            return <InputNumber />
        }
        return <Input />
    }

    render() {
        const {
            editing,
            dataIndex,
            title,
            inputType,
            record,
            index,
            ...restProps
        } = this.props
        return (
            <EditableContext.Consumer>
                {form => {
                    const { getFieldDecorator } = form
                    return (
                        <td {...restProps}>
                            {editing ? (
                                <FormItem style={{ margin: 0 }}>
                                    {getFieldDecorator(dataIndex, {
                                        rules: [
                                            {
                                                required: true,
                                                message: `请添加 ${title}!`
                                            }
                                        ],
                                        initialValue: record[dataIndex]
                                    })(this.getInput())}
                                </FormItem>
                            ) : (
                                restProps.children
                            )}
                        </td>
                    )
                }}
            </EditableContext.Consumer>
        )
    }
}

@inject('store')
@observer
class ProjectList extends React.Component {
    constructor(props) {
        super(props)
        this.state = { editingKey: '' }
        this.columns = [
            {
                title: '编号',
                key: 'number',
                dataIndex: 'number'
            },
            {
                title: '项目名称',
                key: 'name',
                dataIndex: 'name',
                editable: true
            },
            {
                title: '产品经理',
                key: 'pd',
                dataIndex: 'pd',
                editable: true
            },
            {
                title: '操作',
                dataIndex: 'operation',
                render: (text, record) => {
                    const editable = this.isEditing(record)
                    return (
                        <div>
                            {editable ? (
                                <span>
                                    <EditableContext.Consumer>
                                        {form => (
                                            <a
                                                href="javascript:;"
                                                onClick={() =>
                                                    this.save(form, record.id)
                                                }
                                                style={{ marginRight: 8 }}
                                            >
                                                保存
                                            </a>
                                        )}
                                    </EditableContext.Consumer>
                                    <Popconfirm
                                        title="确定取消?"
                                        onConfirm={() => this.cancel(record.id)}
                                    >
                                        <a>取消</a>
                                    </Popconfirm>
                                </span>
                            ) : (
                                <a onClick={() => this.edit(record.id)}>编辑</a>
                            )}
                            &nbsp;&nbsp;|
                            <span className={styles.delete}>
                                <Popconfirm
                                    title="确定删除?"
                                    onConfirm={() => this.delete(record.id)}
                                >
                                    <a>删除</a>
                                </Popconfirm>
                            </span>
                        </div>
                    )
                }
            }
        ]
    }

    isEditing = record => record.id === this.state.editingKey

    // 取消功能
    cancel = () => {
        this.setState({ editingKey: '' })
    }

    // 保存功能
    save(form, key) {
        form.validateFields((error, row) => {
            if (error) {
                return
            }
            const newData = [...toJS(this.props.store.projectList)]
            const index = newData.findIndex(item => key === item.id)
            if (index > -1) {
                const item = newData[index]
                newData.splice(index, 1, {
                    ...item,
                    ...row
                })
                this.setState({ editingKey: '' }, () => {
                    this.props.store.onSave(newData, () => {})
                })
            } else {
                newData.push(row)
                // this.setState({ data: newData, editingKey: '' });
            }
        })
    }

    // 是否编辑字段
    edit(key) {
        this.setState({ editingKey: key })
    }

    // 删除功能
    delete = key => {
        this.props.store.onDelete(key, () => {
            message.info('删除成功')
        })
    }

    // 添加功能
    onProjectAdd = () => {
        this.props.store.onAdd()
    }

    // 数据打印功能
    onProjectSave = () => {
        this.props.store.onSaveAllData(value => {
            console.log(toJS(value))
        })
    }

    render() {
        const components = {
            body: {
                row: EditableFormRow,
                cell: EditableCell
            }
        }

        const columns = this.columns.map(col => {
            if (!col.editable) {
                return col
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    inputType: col.dataIndex === 'age' ? 'number' : 'text',
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record)
                })
            }
        })

        const projectList = toJS(this.props.store.projectList)
        const dataSource = projectList

        return (
            <Card>
                <div>
                    <Table
                        rowKey={record => record.id}
                        components={components}
                        bordered
                        dataSource={dataSource}
                        columns={columns}
                        pagination={false}
                    />
                    <br />
                    <br />
                    <Button
                        style={{
                            width: '100%',
                            marginTop: 16,
                            marginBottom: 8
                        }}
                        type="dashed"
                        icon="plus"
                        onClick={this.onProjectAdd}
                    >
                        添加
                    </Button>
                    <Button
                        style={{
                            width: '100%',
                            marginTop: 16,
                            marginBottom: 8
                        }}
                        type="dashed"
                        icon="plus"
                        onClick={this.onProjectSave}
                    >
                        数据打印
                    </Button>
                </div>
            </Card>
        )
    }

    componentDidMount() {
        this.props.store.getProjectList()
    }
}

ProjectList.defaultProps = {}
export default ProjectList
