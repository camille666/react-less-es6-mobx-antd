import { observable, action, runInAction } from 'mobx'
import axios from 'axios'

class Store {
    @observable projectList = []

    @action async getProjectList() {
        const res = await axios.get('/api/projectList')
        if (res.data.success) {
            const data = res.data.data
            runInAction(() => {
                this.projectList = data
            })
        }
    }

    @action async onDelete(value, callback) {
        const res = await axios.post('/api/deleteProject', { id: value })
        if (res.data.success) {
            this.projectList = this.projectList.filter(item => {
                return item.id !== value
            })
            callback && callback()
        }
    }

    @action async onSave(value, callback) {
        this.projectList = value
        callback && callback()
    }

    @action onAdd() {
        let maxNum
        if (this.projectList.length) {
            maxNum = Math.max.apply(
                Math,
                this.projectList.map(function(o) {
                    return o.number
                })
            )
        } else {
            maxNum = 0
        }
        this.projectList.push({
            number: maxNum + 1,
            name: '',
            pd: '',
            developer: '',
            remark: '',
            id: maxNum + 1
        })
    }

    @action async onSaveAllData(callback) {
        const res = await axios.post('/api/saveProject', {
            item: this.projectList
        })
        if (res.data.success) {
            callback && callback(this.projectList)
        }
    }
}

export default Store
