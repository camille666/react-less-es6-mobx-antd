import React from 'react'
import { Provider } from 'mobx-react'
import Store from './store'

import ProjectList from './projectList'
import styles from './project.less'
const store = new Store()

class Project extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <div className={styles.box}>
                    <ProjectList />
                </div>
            </Provider>
        )
    }
}
Project.defaultProps = {}
export default Project
