module.exports = {
    personList: require('./data/personList'),
    delPerson: require('./data/delPerson'),
    updatePerson: require('./data/updatePerson'),
    projectList: require('./data/projectList'),
    deleteProject: require('./data/deleteProject'),
    saveProject: require('./data/saveProject')
}
