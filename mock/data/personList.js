module.exports = {
    message: null,
    data: [
        {
            number: 1,
            name: '袁宏',
            sex: '男',
            remark:
                '功底较好，深沉，内向，非常靠谱，认真，懂得思考，总结，自省；技术深度和广度需要加强'
        },
        {
            number: 2,
            name: '张凤旗',
            sex: '女',
            remark:
                '有一定的抗压能力，具备初步项目管理能力，学习能力不错；总结和思考需要加强'
        },
        {
            number: 3,
            name: '骆文帅',
            sex: '男',
            remark: '有一定的抗压能力，学习能力不错；沟通总结思考需要加强'
        },
        {
            number: 4,
            name: '谢永庆',
            sex: '男',
            remark: '主动思考，乐于尝试；总结抽象自省需要加强'
        },
        {
            number: 5,
            name: '闫泽佳',
            sex: '男',
            remark: '乐观，有要性，责任心，抗压能力；思考，总结需要加强'
        },
        {
            number: 6,
            name: '章英霞',
            sex: '女',
            remark: '乐观，技术能力不错；主动沟通，思考，总结需要加强'
        },
        {
            number: 7,
            name: '李硕',
            sex: '男',
            remark: '乐观，有要性，责任心，抗压能力；思考，总结需要加强'
        }
    ],
    success: true
}
