module.exports = {
    message: null,
    data: [
        {
            number: 1,
            name: 'crm系统',
            pd: '杜晓黎',
            developer: '张凤旗，谢永庆',
            remark: '这是一个全新的系统，会用mobx模版，技术栈：react + mobx',
            id: 1
        },
        {
            number: 2,
            name: '视频结构化',
            pd: '王新志',
            developer: '光波',
            remark: '目前处于探索阶段，参考百度的视频解析。',
            id: 2
        },
        {
            number: 3,
            name: '大屏',
            pd: '徐超',
            developer: '胖歌，袁宏',
            remark: '这是330要上线的saas大屏，会用发消息模版，技术栈：react。',
            id: 3
        },
        {
            number: 4,
            name: '平台管理',
            pd: '周振中',
            developer: '顾少华，闫泽佳',
            remark: '这是308要上线的saas需求，中间闫泽佳可能会去支持大屏开发。',
            id: 4
        },
        {
            number: 5,
            name: '预警中心',
            pd: '孟志高',
            developer: '骆文帅',
            remark: '这是308要上线的saas需求，fk，bq有个性化',
            id: 5
        },
        {
            number: 6,
            name: '大禹平台',
            pd: '伍强',
            developer: '章英霞',
            remark:
                '这是迭代老项目，旧架构，希望兼顾旧功能，bug少一些，代码质量高一些。',
            id: 6
        },
        {
            number: 7,
            name: '开源情报系统',
            pd: '刘一新',
            developer: '李硕',
            remark: '1季度需求较简单，界面重构在2季度开始。',
            id: 7
        }
    ],
    success: true
}
