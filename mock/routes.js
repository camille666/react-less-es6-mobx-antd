module.exports = {
    '/api/personList*': '/personList',
    '/api/delPerson*': '/delPerson',
    '/api/updatePerson*': '/updatePerson',
    '/api/projectList*': '/projectList',
    '/api/deleteProject*': '/deleteProject',
    '/api/saveProject*': '/saveProject'
}
